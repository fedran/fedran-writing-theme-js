{
  description = "A theme for mfgames-writing-js for books written in the Fedran universe.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell =
          pkgs.mkShell { buildInputs = [ pkgs.nixfmt pkgs.nodejs-16_x ]; };
      });
}
