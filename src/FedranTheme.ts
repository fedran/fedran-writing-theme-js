import { ContentArgs, ContentTheme } from "@mfgames-writing/contracts";
import * as liquid from "@mfgames-writing/liquid-theme";
import * as path from "path";

export class FedranTheme extends liquid.LiquidTheme {
    constructor() {
        let templateDirectory = path.join(__dirname, "..", "templates");
        let styleDirectory = path.join(__dirname, "..", "styles");
        let assetsDirectory = path.join(__dirname, "..", "assets");

        super(templateDirectory, styleDirectory, assetsDirectory);
    }

    public getContentTheme(content: ContentArgs): ContentTheme {
        // Start with the defaults.
        var results = super.getContentTheme(content);

        // Figure out the defaults for types.
        switch (content.element) {
            case "cover":
            case "legal":
            case "title":
            case "bastard":
            case "dedication":
            case "toc":
            case "preface":
                results.styleName = "blank";
                break;
        }

        // Return the results.
        return results;
    }

    public transformHtml(content: ContentArgs, html: string): string {
        const chapter = new RegExp(
            [
                "^<blockquote>",
                "(.*?)\\s*(—|---)\\s*(.*?)",
                "</p>.*?",
                "</blockquote>\\s*<p>",
                "(\\W)?(\\w)([^\\s]*)",
            ].join(""),
            "s"
        );
        const epigraph = html.replace(
            chapter,
            [
                "<blockquote class='epigraph'>$1</p><p class='attribution'>$2$3</p></blockquote>",
                "<p class='first'>",
                "<span class='first-word'><span class='first-letter'>$4$5</span>$6</span>",
            ].join("")
        );

        return epigraph;
    }

    public renderEmphasis(_: ContentArgs, text: string): string {
        const replacedSpaces = text.replace(/(\s+)/g, "</em>$1<em>");

        return "<em>" + replacedSpaces + "</em>";
    }
}
