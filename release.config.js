module.exports = {
    branches: ["main"],

    extends: ["@commitlint/config-conventional"],
    message:
        "chore(release): v${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",

    plugins: [
        [
            "@semantic-release/commit-analyzer",

            {
                preset: "conventionalcommits",
            },
        ],
        "@semantic-release/release-notes-generator",
        "@semantic-release/npm",
        [
            "@semantic-release/exec",
            {
                prepareCmd: "npm run build",
            },
        ],
        "@semantic-release/changelog",
        "@semantic-release/git",

        ["@semantic-release/gitlab", {}],
    ],
};
